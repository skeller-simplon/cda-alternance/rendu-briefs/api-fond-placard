package com.consulner.app;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;

import com.sun.net.httpserver.HttpServer;

class Application {

    public static void main(String[] args) throws IOException {
        int serverPort = 8000;
        HttpServer server = HttpServer.create(new InetSocketAddress(serverPort), 0);
        
        server.createContext("/api/", (exchange -> {
            URI uri = exchange.getRequestURI();
            System.out.print(uri.getPath()); 

        }));
        server.setExecutor(null); // creates a default executor
        server.start();

    }
}